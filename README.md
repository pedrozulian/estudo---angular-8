# Repositório

Repositório relacionado ao curso de Angular CLI disponível na Udemy. 
A ideia é criar o hábito de utilizar um gerenciador de repositório (Gitlab), específicar todas as etapas de aprendizado do curso e deixar disponível todo o conteúdo para quem tenha o curso completo ou para atividades extras como: compartilhar conhecimento (workshop, treinamentos), projetos acadêmicos ou projetos pessoais.


# Apoio 

No repositório 'Anotações', temos especificações sobre todas as ferramentas que serão usadas durante o desenvolvimento e suas respectivas funcionalidades. No 'Estrutura Angular CLI', há toda explicação de todas as frações das aplicações (função de cada módulo).